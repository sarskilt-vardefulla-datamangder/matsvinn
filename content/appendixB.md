# Exempel i JSON

Samma entitet som i Appendix A, fast nu som JSON.

```json
{
    "source":  "2120001637",
    "id":  "2637-ms-001",
    "name":  "Algården",
    "date":  "2023-10-11",
    "eating": "31",
    "serving": "0.9",
    "plate":  "10.9",
    "kitchen":  "3.5",
    "storage":  "0.9",
    "preparation":  "1.5",
    "cooking":  "1.2",
    "diet":  "2.4",
    "total":  "5.3",
    "category":  "PS",
    "operation":  "receiving",
    "meal":  "lunch",
    "prepared":  "43",
    "saved":  "0.6",
    "description":  "Första försök på manuell rapportering",
}