**Bakgrund**

Syftet med denna specifikation är att beskriva information om entiteten på ett enhetligt och standardiserat vis. 

Specifikationen syftar till att ge kommuner i Sverige möjlighet att enkelt kunna sätta samman och publicera datamängd(er) som beskriver entiteter. Den syftar även till att göra det enklare för internationella användare som är tagare av datamängden.

## Författare

Mattias Axell - Nationell Dataverkstad<br>
Filip Dijak - Nationell Dataverkstad<br>

## Med bidrag från

**[Nationell Dataverkstad](https://www.vgregion.se/ov/dataverkstad/)** - Modellering och rådgivning.<br>
Karin Fritz - [Livsmedelsverket](https://www.livsmedelsverket.se/)<br>
Per Jonsson - [Livsmedelsverket](https://www.livsmedelsverket.se/)<br>
Sandra Sjöholm - [Södertälje kommun](https://sodertalje.se/)<br>
Maria Söderlind - [Umeå kommun](https://umea.se/)<br>
Åsa Cannerheim - [Södertälje kommun](https://sodertalje.se/)<br>
Andreas Sundberg - [Södertälje kommun](https://sodertalje.se/)<br>
Örjan Fallheden - [Södertälje kommun](https://sodertalje.se/)<br>
Johan Hultén - [IVL Svenska Miljöinstitutet](https://www.ivl.se/)<br>
Matthias Palmér - [MetaSolutions AB](https://metasolutions.se/)<br>
Sari Nieminen - [CGI (Aromi)](https://www.cgi.com/)<br>
Eveliina Lindell - [CGI (Aromi)](https://www.cgi.com/)<br>
Lena Schröder - [Matilda Foodtech](https://www.matildafoodtech.com/)<br>
Pontus Albinzon - [IST](https://www.ist.com/)<br>
Linda Rydén - [eSmiley](https://www.esmiley.se/)<br>
Teemu Lankila - [SmartKitchen Solutions](https://smartkitchen.solutions/)<br>
Olof Dunsö - [Naturvårdsverket](https://www.naturvardsverket.se/)<br>