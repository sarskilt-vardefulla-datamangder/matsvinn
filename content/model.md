# Datamodell

Datamodellen är tabulär vilket innebär att varje rad motsvarar exakt en rapportering av matsvinn och varje kolumn motsvarar en egenskap för det matsvinnet. Totalt är 19 attribut definierade där de första 4 är obligatoriska.

<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska. Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.
</div>

<div class="note" title="2">
Beroende på typ av kök så kan vissa typer av matsvinn utebli. T.ex. ett serveringskök tar emot tillagad mat och serverar det enbart, det sker ingen matlagning i köket och kan således inte ha kökssvinn. Ange nollvärde som 0 eller 0.0. 
</div>

<div class="note" title="3">
I datamodellen finns två olika typer av kök vilket bygger på systemleverantörernas definitioner. Ange den typ av kök, som passar bäst in på er verksamhet.
</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**source**](#source)|1|[heltal](#heltal)|**Obligatoriskt** - Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen som hanterar matsvinnet. |
|[**id**](#id)|1|text|**Obligatoriskt** - Anger en unik och stabil identifierare för verksamhetens matsvinn. |
|[**name**](#name)|1|text|**Obligatoriskt** - Anger namn på verksamheten där matsvinn skapakts. |
|[**date**](#date)|1|[dateTime](#datetime)| **Obligatorisk** - Ange datumet för det år, månad eller dag som det registrerade svinnet avser enligt [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html). Anges per år som YYYY, per månad som YYYY-MM eller per dag som YYYY-MM-DD. |
|[eating](#eating)|0..1|heltal|**Rekommenderat** - Anger antalet personer som ätit under den perioden då matsvinn skapats. Detta värde bör alltid tillhandahållas om det är möjligt. |
|[serving](#serving)|0..1|decimal|**Rekommenderat** - Anger serveringssvinn i kilogram med en decimal. Detta värde bör alltid tillhandahållas om det är möjligt. |
|[plate](#plate)|0..1|decimal|**Rekommenderat** - Anger tallrikssvinn i kilogram med en decimal. Detta värde bör alltid tillhandahållas om det är möjligt. |
|[kitchen](#kitchen)|0..1|decimal|**Rekommenderat** - Anger kökssvinn i kilogram med en decimal. Detta värde bör alltid tillhandahållas om det är möjligt. |
|[storage](#storage)|0..1|decimal|Anger lagringssvinn i kilogram med en decimal. |
|[preparation](#preparation)|0..1|decimal|Anger beredningssvinn i kilogram med en decimal. |
|[cooking](#cooking)|0..1|decimal|Anger tillagningssvinn i kilogram med en decimal. |
|[diet](#diet)|0..1|decimal|Anger mängden specialkostsvinn i antal kilogram med en decimal. |
|[total](#total)|0..1|decimal|Anger total mängd matvsinn i kilogram med en decimal. |
|[category](#category)|0..1|PS &vert; ES &vert; HS &vert; H &vert; P &vert; R &vert; N &vert; C &vert; O|Anger kategori på verksamhet som skapat matsvinn, t.ex. förskola (PS). Läs mer under [category](#category). |
[operation](#operation)|0..1|(cooking &vert; receiving)|Anger typ av kök vid verksamheten; cooking (_tillagningskök_) eller receiving (_mottagningskök_). Notera att typ av kök kan påverka mängden svinn. |
|[meal](#meal)|0..1|B &vert; S &vert; L &vert; D|Anger vilken typ av måltid som serverats. Läs mer under [meal](#meal). |
|[prepared](#prepared)|0..1|decimal|Anger total mängd preparerad mat i kilogram med en decimal. |
|[saved](#saved)|0..1|decimal|Anger sparad mat i antal kilogram med en decimal. |
|[description](#description)|0..1|text|Anger en kortare beskrivning av rapporterat matsvinn. |

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, d.v.s. inga inledande eller eftersläpande tecken tillåts.

### **heltal**

Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**

Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där matsvinnen presenteras hos den lokala myndigheten eller arrangören.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

</div>

## Förtydligande av attribut

### **source**

Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen. Exempel för Gullspångs kommun: **2120001637**.

### **id**

Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

Observera att “id” inte är namnet på verksamheten som registrerat matsvinnet utan dess identifierare. I fältet “name” skriver du in precis hur du vill att matsvinnets ursprung ska representeras i text. “id” är enbart till för att skapa en stabil identifiering som används när man ska låta en dator hantera informationen. Du kan jämföra attributen “id” och “name” som personnummer och namn för en människa. "id" är personnummer och identifierar exakt en person. "name" är namn på personen och flera människor kan heta Ellen Ripley, men alla har ett unikt personnummer. Det är viktigt att detta attribut är stabilt över tid, att det aldrig ändras. 

Om du redan har en unik och stabil identifierare för din verksamhets matsvinn i ett eget system, t.ex. ett kostplaneringssystem,  eller motsvarande, kan du använda denna istället som ID. Du ansvarar för att namnet är unikt och stabilt över tid. Ett ID från ett verksamhetssystem inom kommunen duger gott, om du tillser att det är just unikt. Tre exempel på ID:n från verksamhetssystem inom en fiktiv kommun:

Om du inte har en unik och stabil identifierare för din verksamhets matsvinn redan, rekommenderas du att skapa en genom att ange en kommunkod från SCB för den kommun där verksamheten finns, följt av ett bindestreck “-” och namnet på verksamheten utan mellanslag och utan svenska tecken. Använd A-O och a-o, inga apostrofer, accenter, skiljetecken. Kommunkod anges alltid med fyra siffror med inledande nolla om sådan finns:
[kommunkod]-[NamnUtanSvenskaTeckenEllerMellanslag]

Uttryckt som ett reguljärt uttryck blir detta: /^\d{4}-[a-zA-Z]+$/

Exempel “0580-katedralskolan”

### **name**

Anger namn på verksamheten där matsvinn skapats. Exempelvis "Algården" eller "Katedralskolan".

### **date**

Ange perioden för den dag som det registrerade svinnet avser enligt internationella standarden [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html). Det är viktigt att kunna härleda för vilket år, vilken månad eller vilken dag som matsvinnet registrerats, och om det registreras kontinuerligt kunna följa trender och hitta samband.

### **eating**

Anger antalet ätande för tillfället då matsvinnet registrerades. 

Ett sätt att räkna ut antalet ätande personer, t.ex. elever, är att räkna antalet tallrikar i disken.

### **serving**

Serveringsvinn avser det svinn som tillkommit under servering av måltider. Om det inte kan rapporteras serveringssvinn ska detta lämnas blankt. Observera att ett nollvärde anges enligt 0 eller 0.0, båda två accepteras, och att utelämnat värde aldrig skall tolkas som ett nollvärde utan som "avsaknad" av värde. Att ett värde saknas kan bero på flera saker t.ex. att verksamheten inte vägt svinn för den tidpunkten eller att det är ett sådant typ av kök som inte kan påvisa denna typ av svinn. 

### **plate**

Tallriksvinn avser det svinn som ätande lämnat kvar på tallriken och som oftast skrapas ner i sopsäckar. Observera att ett nollvärde anges enligt 0.0, och att utelämnat värde aldrig skall anges som ett nollvärde utan som "avsaknad" av värde. Att ett värde saknas kan bero på flera saker t.ex. att verksamheten inte vägt svinn för den tidpunkten eller att det är ett sådant typ av kök som inte kan påvisa denna typ av svinn. 

### **kitchen**

Kökssvinn, även kallat tillagningssvinn som tillkommit under tillagning. Observera att ett nollvärde anges som 0, och att utelämnat värde aldrig skall tolkas som ett nollvärde utan som "avsaknad" av värde. Att ett värde saknas kan bero på flera saker t.ex. att verksamheten inte vägt svinn för den tidpunkten eller att det är ett sådant typ av kök som inte kan påvisa denna typ av svinn. 

### **storage**

Lagringsvinn, beredningssvinn & tillagningssvinn är tre svinntyper som ingår i det som definieras kökssvinn. Kökssvinnet, kan aldrig bli högre än summan av dessa tre fält. Lagringssvinn uppstår i samband med lagring av livsmedel i köket. T.ex. skulle det kunna vara livsmedelsprodukter där bäst-före-datumet gått ut och det inte går att tillreda samt servera så det behöver slängas från kylar, frysar och förråd. Genom att lägga till detta attribut möjliggörs analys av matsvinn för en större del av tillagningskedjan. 

### **preparation**

Lagringsvinn, beredningssvinn & tillagningssvinn är tre svinntyper som ingår i det som definieras kökssvinn. Kökssvinnet, kan aldrig bli högre än summan av dessa tre fält. Anger det svinn som uppstår i samband med beredning av måltider, men inte är dålig utan hade kunnat ätas. Det här ett exempel på svinn som inte kan uppstå i alla typer av kök. 

### **cooking**

Lagringsvinn, beredningssvinn & tillagningssvinn är tre svinntyper som ingår i det som definieras kökssvinn. Kökssvinnet, kan aldrig bli högre än summan av dessa tre fält. Tillagningsvinn är mat som tillagas men inte tas tillvara av olika anledningar och slängs utan att ha serverats. OBSERVERA att överbliven mat som sparas för servering vid senare tillfälle, se saved_food, ska inte räknas med.  

### **diet**

Ange totala mängden svinn som kommer från specialkost. Det är allt fler ätande som har olika kostpreferenser och allergier som gör att antalet tallrikar med specialkost ökar. Specialkost är hos flera verksamheter också förknippat med relativt höga omkostnader - så det finns en ekonomisk nytta i att kunna följa och analysera det.

### **total**

Anger total mängd matsvinn. Det här går att beräkna genom att addera de olika svinntyperna, men för vissa tagare av denna data är enbart det totala matsvinnet intressant - och finns det i ett eget fält är det enklare att hämta just det via ett API. 

### **category**

Anger vilken typ av verksamhet som matsvinnet härstammar ifrån. PS (Preschool, _förskola_), ES (ElementarySchool _GrundSkola_), HS, (HighSchool, _GymnasieSkola_) H (Hospital, _sjukhus_), P (Detention centres and prisons, _häkten och fängelser_), R (restaurant, _restaurang_), N (nursing and care home, _vård- och omsorgsboende_), C (ChildCare _FamiljeDaghem_) och O (other operations, _annan verksamhet_).

### **operation**

Detta attribut är inte obligatoriskt men starkt rekommenderat att dela eftersom det bidrar till förståelse för matsvinnet, då matsvinn kan variera i mängd beroende på typ av kök. De typer som finns definierade är cooking (_tillagningskök_), och receiving (_mottagningskök_)

cooking - ett _tillagningskök_ är ett kök som är utrustade och anpassade för att ha möjlighet att tillaga mat från grunden. Dessa kan ha intilligande serveringsdel, men det behöver inte vara så. Dessa kök kan enbar leverera mat till flera olika, ibland geografiskt spridda enheter. Ofta tillagas då delar av måltiden av mottagningskök som kompletterar med resterande måltidstillbehör. Observera att det kan finnas lokala benämningar på tillagningskök som t.ex. produktionskök eller centralkök. Dessa ska klassificeras som tillagningskök.

receiving - ett _mottagningskök_ är ett kök med begränsat möjlighet att tillaga mat. Beroende på de förutsättningar som finns kan det fungera likt ett tillagningskök med vissa undantag eller vara mer begränsat och endast bereda sallader och kalla såser exempelvis. Ett mottagningskök kan ta emot varm, kyld eller fryst mat för vidare beredning och mottagningsköket kan ha en lokal restaurang eller serveringsdel i anslutnings till köket.

Eftersom skillnaden är så liten på ett mottagningskök och serveringskök så har vi valt att slå ihop dem till "mottagningskök". Detta delvis eftersom det är så Livsmedelsverket samlar in sin statistik via enkäter. Men för att förtydliga så är ett serveringskök ett kök med små möjligheter att tillaga mat. Dessa har vanligtvis liten eller ingen möjlighet till beredning av den mat som serveras p.g.a lokalens utformning eller brist på utrustning. Därför levereras ofta kompletta måltider som är redo att serveras, men i vissa fall kanske det finns en litet utrymme att bereda sallad eller något mindre. Till serveringskök kan man ofta räkna in t.ex. avdelningskök på förskola, äldreboende eller sjukhusavdelningar.

Detta innebär att vissa typer av kök inte kan rapportera in alla typer av matsvinn.

### **meal**

Anger vilken typ av måltid som serverats, det ska vara möjligt att ange B (Breakfast, _frukost_), S (Snack, _mellanmål_), L (Lunch, _lunch_) eller D (Dinner, _middag_). 

### **prepared**

Anger total mängd preparerad mat. Den här attributet är viktigt i samspel med mängden sparad mat, således möjliggörs att räkna ut den totala mängden konsumerat mad men också det totala mängden svinn. För många tagare av matsvinns-data är det av intresse att också kunna analysera hur mycket näring som faktiskt levereras via t.ex de offentliga köken. 

### **saved**

Anger mängden sparad mat i antal kilogram. Detta är intressant för kök som t.ex. under en dag kan förbereda en stor mängd mat, men all mat ska inte serveras och det blir därmed konstigt om det ska registreras som svinn. Viss mat sparas till nästkommande dag, och då är det här attributet ett bra sätt att uttrycka den sparade mängden mat för att få en mer korrekt rapportering av det faktiska svinnet. 

### **description**

Anger en beskrivning av matsvinnet. En möjlighet att ange om det finns en specifik anledning till ett högt matsvinn t.ex. en hög oanmäld frånvaro eller något annat.