# Introduktion

Med matsvinn avses livsmedel som har producerats i syfte att bli mat men som av olika anledningar inte äts upp. Matsvinnet kan uppkomma i hela livsmedelskedjan men i den här specifikationen avgränsar vi oss till det matsvinn som uppstår i köken. Anledningar till matsvinn kan vara många i köken, allt från att livsmedel tappats på golvet, livsmedel vars bäst före datum har passerat, konkreta rester som slängs et.c. 

I genomsnitt slängs det årligen 127 kg livsmedelsavfall per person i Sverige. Att mäta avfallet och identifiera var det uppstår, ger organisationer möjligheten att kunna sätta in åtgärder. På så sätt kan vi bidra till en minskad miljö- och klimatpåverkan. Om vi utgår från avfallstrappan; minimera, återanvända, återvinna, utvinna energi och deponera - så är hela syftet med att dela data om matsvinn att identifiera hur mängden matsvinn faktiskt kan minimeras. Det leder också till minskade inköpskostnader för organisationen. 

Data om matsvinn är inte heller bara intressant för organisationen, utan det finns en stor efterfrågan hos externa parter. 5 juli 2023 presenterade EU-kommissionen ett [revideringsförslag](https://environment.ec.europa.eu/publications/proposal-targeted-revision-waste-framework-directive_en ) av det befintliga ramdirektivet för avfall med fokus på matsvinn och textilier. I det nya förslaget föreslås det att matsvinnet från tillverkning och beredning av livsmedel ska minska med 10% till 2030.

I Sverige så har [Livsmedelsverket](https://www.livsmedelsverket.se/matvanor-halsa--miljo/matsvinn), [Naturvårdsverket](https://www.naturvardsverket.se/amnesomraden/avfall/avfallslag/matavfall-och-matsvinn/) och [Jordbruksverket](https://www.naturvardsverket.se/amnesomraden/avfall/avfallslag/matavfall-och-matsvinn/) ett [gemensamt regeringsuppdrag att minska Sveriges matsvinn](https://www.naturvardsverket.se/om-oss/aktuellt/nyheter-och-pressmeddelanden/stort-fokus-pa-att-mata-och-minska-matsvinn-bade-i-sverige-och-eu/). Som en del av uppdraget samlar myndigheterna in data från olika aktörer via enkäter och formulär om deras matsvinn.

Problematiken idag är att data är strukturerad på olika sätt hos verksamheterna, vilket försvårar insamling av data. Syftet med den här specifikationen är att standardisera datadelning om matsvinn. 

Om ni som kommun, region eller myndighet har en systemleverantör för denna data ska ni kunna exportera ut en fil över den rapporterade matsvinner för perioden enligt specifikation. Har ni inget system förutom kalkylark kan ni jobba med den mall som finns.

I arbetet har systemleverantörer, kommuner och relevanta myndigheter involverats.
